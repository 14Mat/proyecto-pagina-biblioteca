//Header
window.addEventListener("scroll", function() {
        var header = document.querySelector("header");
        header.classList.toggle("sticky", window.scrollY > 0)
    })
    //Boton Wpp
$(function() {
    $('#myButton').floatingWhatsApp({
        phone: '+54 9 2994 09-5372',
        popupMessage: '¿En que puedo ayudarte?',
        message: "Necesito ayuda con algo...",
        showPopup: true,
        showOnIE: false,
        headerTitle: 'Bienvenido!',
        headerColor: 'green',
        backgroundColor: 'green',
        buttonImage: '<img src="Imagenes/whatsapp.svg" />'
    });
});
//Google Map
function iniciarMap() {
    var coord = { lat: -38.9350126, lng: -68.1244921 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: coord
    });
    var marker = new google.maps.Marker({
        position: coord,
        map: map
    });
}